# Creating a virtual environment and activation
# python3.11 -m venv venv
# source venv/bin/activate

# Installing Python header files
sudo apt-get update && apt-get install -y python3-dev

# Install Poetry
pip install poetry

# Install dependencies
poetry install

poetry config virtualenvs.create false --local

# Running migrations and static file builds
poetry run python manage.py migrate
poetry run python manage.py collectstatic --no-input
