# Using a basic Python image
FROM python:3.11

# Installing Python header files and other dependencies
RUN apt-get update && apt-get install -y \
    python3-dev \
    libpq-dev \
    gcc \
    && rm -rf /var/lib/apt/lists/*

# Install Poetry
RUN pip install poetry

# Set the working directory in the container
WORKDIR /code

# Copy the pyproject.toml file to the working directory
COPY pyproject.toml poetry.lock ./

# Set dependencies in the container via Poetry
RUN poetry config virtualenvs.create false && \
    poetry install --no-dev --no-interaction --no-ansi

# Copy the rest of the project files
COPY . .
